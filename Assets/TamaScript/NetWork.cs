﻿using UnityEngine;
using System.Collections;

public class NetWork : MonoBehaviour {
	public GameObject prefub;
	public string ip = "localhost";
	
	private string port = "21025";
	private bool connected = false;
	
	private void CreatePlayer()
	{
		prefub.tag = "Player";
		connected = true;
		int positionNo = Random.Range(0, transform.childCount);
		Transform childTransform = transform.GetChild(positionNo).transform;
		GameObject myPlayer =  Network.Instantiate(prefub, childTransform.position, Quaternion.identity, 1) as GameObject;
		myPlayer.name = "MyPlayer";
		
	}
	private void CreatePlayerOffline()
	{
		prefub.tag = "Player";
		connected = true;
		int positionNo = Random.Range(0, transform.childCount);
		Transform childTransform = transform.GetChild(positionNo).transform;
		GameObject myPlayer =  Object.Instantiate(prefub, transform.position, transform.rotation) as GameObject;
		myPlayer.name = "MyPlayer";
		
	}
	
	public void OnDisconnectedFromServer(){ connected = false; }
	
	public void OnPlayerDisconnected(NetworkPlayer pl){ Network.DestroyPlayerObjects(pl);}
	
	
	public void OnConnectedToServer(){ CreatePlayer(); }
	
	public void OnServerInitialized(){ CreatePlayer(); }
	
	
	public void OnGUI()
	{
		if( !connected)
		{
			ip = UnityEngine.GUI.TextField( new Rect(10,10,90,20),ip);
			port = UnityEngine.GUI.TextField( new Rect(10, 40, 90, 20), port);
			
			if( UnityEngine.GUI.Button( new Rect( 10,70,90, 20), "Connect")){ Network.Connect(ip, int.Parse(port) ); }
			if( UnityEngine.GUI.Button( new Rect(10, 100, 90, 20), "host")){ Network.InitializeServer(10, int.Parse(port), false); }
			if( UnityEngine.GUI.Button( new Rect(10, 130, 90, 20), "Ofline")){ CreatePlayerOffline(); }
			
		}
	}
	
}
