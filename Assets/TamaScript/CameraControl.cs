﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	
	private GameObject player = null;
	
	public Vector3 offset;
	
	// Use this for initialization
	void Start () {
//		this.player = GameObject.FindGameObjectWithTag ("Player");
//		this.offset = this.transform.position - this.player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (null == this.player) {
			this.player = GameObject.Find ("MyPlayer");
			if (null == this.player) {
				return;
			}
		}
		this.transform.position = new Vector3 (this.player.transform.position.x + this.offset.x, this.player.transform.position.y + this.offset.y, this.player.transform.position.z + this.offset.z);
		transform.LookAt (this.player.transform.position);
//		float delta = Input.GetAxis ("Mouse ScrollWheel");
//		if (delta != 0.0f) {
//			this.mouseWheelEvent (delta);
//		}
	}
}