﻿using UnityEngine;
using System.Collections;

public class CustomJump : MonoBehaviour
{

	private Animator anim;	
	private AnimatorStateInfo currentBaseState;

	private float speed = 0;
	private float direction = 0;
	private Locomotion locomotion = null;
		
	public float force = 5.0f;
	public float maxJumpCount = 10.0f;	// ジャンプできる最大回数
	public float animSpeed = 1.5f;

	public float jumpCount = 0.0f;		// ジャンプした回数
	private float jumpPower = 0.0f;

	void Start () 
	{
		anim = GetComponent<Animator>();
		locomotion = new Locomotion(anim);
	}
	
	private void Update()
	{
		if (anim && Camera.main)
		{
			JoystickToEvents.Do(transform,Camera.main.transform, ref speed, ref direction);
			locomotion.Do(speed * 6, direction * 180);

			if (jumpCount < maxJumpCount && Input.GetKey ("space"))
			{
//				anim.speed = animSpeed;
				// アニメーション
//				anim.SetBool("Jump", true);	// ジャンプアニメーション再生

				jumpCount++;
				jumpPower = force * 0.001f * (maxJumpCount - jumpCount);
				transform.Translate(Vector3.up * jumpPower);
				
			}
		
		}

//		ジャンプアニメーションのループ防止
//		if(!anim.IsInTransition(0) && !Input.GetKey ("space")) {
		if(!Input.GetKey ("space")) {
//			anim.SetBool("Jump", false);
			jumpCount = 0.0f; // ジャンプ回数初期化
		}
	}
}